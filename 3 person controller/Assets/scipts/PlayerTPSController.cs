﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerTPSController : MonoBehaviour
{

    public Camera camera;
    public UnityEvent onInteractionInput;
    private InputData input;
    private CharacterAnimBAsedMovement CharacterAnimBAsedMovement;

    public bool onInteractionZone { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        CharacterAnimBAsedMovement = GetComponent<CharacterAnimBAsedMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        input.getInput();

        if (onInteractionZone && input.jump)
        {
            onInteractionInput.Invoke();
        }
        else

        CharacterAnimBAsedMovement.moveCharacter(input.hMovement, input.vMovement, camera, input.jump, input.dash);
    }
}
