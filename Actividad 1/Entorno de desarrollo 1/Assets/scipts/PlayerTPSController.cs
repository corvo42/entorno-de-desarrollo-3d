﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTPSController : MonoBehaviour
{

    public Camera camera;
    private InputData input;
    private CharacterAnimBAsedMovement CharacterAnimBAsedMovement;

    // Start is called before the first frame update
    void Start()
    {
        CharacterAnimBAsedMovement = GetComponent<CharacterAnimBAsedMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        input.getInput();

        CharacterAnimBAsedMovement.moveCharacter(input.hMovement, input.vMovement, camera, input.jump, input.dash);
    }
}
