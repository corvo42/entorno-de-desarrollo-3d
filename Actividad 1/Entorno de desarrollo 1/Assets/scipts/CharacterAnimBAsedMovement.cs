﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]


public class CharacterAnimBAsedMovement : MonoBehaviour
{

    public float rotationSpeed = 4f;
    public float rotationThreshold = 0.3f;
    [Range(0,100f)]
    public float degreesToTurn = 160f;

    [Header("Animation Smoothing")]
    [Range(0, 1f)]
    public float startAnimTime = 0.3f;
    [Range(0, 1f)]
    public float stopAnimTime = 0.15f;

    private Ray wallRay = new Ray();
    private float Speed;
    private Vector3 desiredMovementDirection;
    private CharacterController characterController;
    private Animator animator;
    private bool mirrorIdle;
    private bool turn180;
    public string motionParam;
    public string mirroridleParam;
    public string turn180Param;
    public bool animatorParam;
    

    



    void Start()
    {
    characterController = GetComponent<CharacterController>();
    animator = GetComponent<Animator>();
    }

    public void moveCharacter(float hInput, float vInput, Camera camera, bool jump, bool dash)
    {
        Speed = new Vector2(hInput, vInput).normalized.sqrMagnitude;

        if(Speed >= Speed - rotationThreshold && dash){
            Speed = 1.5f;
        }

        if (Speed > rotationThreshold) {
            animator.SetFloat(motionParam, Speed, startAnimTime, Time.deltaTime);
            Vector3 foward = camera.transform.forward;
            Vector3 right = camera.transform.right;

            foward.y = 0f;
            right.y = 0f;

            foward.Normalize();
            right.Normalize();

            desiredMovementDirection = foward * vInput + right * hInput;
            
            if (Vector3.Angle(transform.forward, desiredMovementDirection) >= degreesToTurn){
                turn180 = true;
            }

            else {
                turn180 = false;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMovementDirection), rotationSpeed * Time.deltaTime);
            }

            animator.SetBool(turn180Param, turn180);
            animator.SetFloat(motionParam, Speed, startAnimTime, Time.deltaTime);

            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMovementDirection), rotationSpeed * Time.deltaTime);

        }

        else if (Speed < rotationThreshold){
            animator.SetBool(mirroridleParam, mirrorIdle);
            animator.SetFloat(motionParam, Speed, stopAnimTime, Time.deltaTime);
        }
        
    }
    private void OnAnimatorIK(int layerIndex){
        if (Speed < rotationThreshold) return;

        float distanceToLeftFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.LeftFoot));
        float distanceToRightFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.RightFoot));

        if (distanceToRightFoot > distanceToLeftFoot) {
            mirrorIdle = true;

        }
        else {
            mirrorIdle = false;
        }
    }
    
}  


